---
title: "Comment j'ai créé ce blog"
date: "2019-09-08"
draft: false
path: "/tech-hub/how-i-made-this-blog"
---

Il y a plein de manières aujourd'hui de créer et de maintenir un blog facilement. Le créer de zéro, partir d'un template, utiliser un CMS, quelque soit votre niveau technique, vous trouverai dans l'écosystème des outils de création de contenue votre bonheur. J'ai choisi pour le mien un "juste milieu": un template Gatsby.js. Et vous voudrais vous le présenter dans ce court article. 

![Logo Gatsby.js](https://res.cloudinary.com/dms9ldi2g/image/upload/v1567964160/blog/gatsby-logo_srkxrk.jpg "Logo Gatsby.js")

##  Gatsby, présentez vous !

Gatsby.js et une "surcouche de React.js, un des framework javascript les plus populaire actuellement pour la fabrication de Single Page Application (SPA). J'ai déjà utilisé React.js dans un précédant job, et je voulais continuer à le pratiquer à titre personnel alors que mon poste actuel ne m'en donne pas la possibilité. Gatsby est l'outil idéal pour les site que je considère comme "statique", mais à contenue "dynamique", c'est à dire des sites avec beaucoup de page ayant le même aspects, mais un contenue différent. C'est typiquement le cas dans blog dont toutes les posts se ressemble par la forme, mais diffère par le fond.

L'énorme bénéfice de Gatsby est ça capacité à faire venir ce contenue d'une multitude de source: des CMS comme [Contentful](https://www.contentful.com/), de ficher json/markdown/csv, de base données, etc... et de le transformé en des composants React.js prêts à l'emploi (un composant react est une brique logique écrite en javascript capable de créer et de manipuler du HTML/CSS en temps réel. Je vous invite à regarder la doc de [React.js](https://fr.reactjs.org/) pour plus de détails). 

Le template que j'ai utilisé est celui de [gatsby-starter-julia](https://www.gatsbyjs.org/starters/niklasmtj/gatsby-starter-julia/) légèrement customisé.

##  Un peu de code 

Le répertoire projet ressemble à ça:

![Project Folder](https://res.cloudinary.com/dms9ldi2g/image/upload/v1567965725/blog/blog-folder_ukftj8.png "Project Folder")

Il est assez similaire à ceux que l'on attend d'un répertoire React.js classique. Mais on remarque quand même la présence ici de 3 fichiers supplémentaires: gatsby-brower.js, gatsby-config.js et gatsby-node.js. Tous ces fichiers sont requis par le template pour son bon fonctionnement. Je ne parlerai pas ici des deux premiers. Je préfère me concentrer dans cette article sur _gatsby-node.js_. Le repertoire **src** contient mes fichiers javascript (classique) et le répertoire **content** contient lui les fichiers markdown qui sont ni plus ni moins que mes articles. **public** est le répertoire de build, ici le répertoire ou sera sauvegardé le site final pour être servi par un serveur web.   

Commençons par jeter un coup d'ceil au fichier _gatsby-node.js_. Le plus important dans ce fichier est la fonction qui permet la création de pages:

```javascript
exports.createPages = ({ graphql, actions }) => {
  const { createPage } = actions
  const blogPostTemplate = path.resolve(`src/templates/blog-post.js`);
  return graphql(`
    {
      allMarkdownRemark {
        edges {
          node {
            frontmatter {
              path
            }
            fields {
              slug
            }
          }
        }
      }
    }
  `
  ).then(result => {
    if(result.errors) {
      return Promise.reject(result.errors)
    }
    result.data.allMarkdownRemark.edges.forEach(({ node }) => {
      createPage({
        path: node.frontmatter.path,
        component: blogPostTemplate,
        slug: node.fields.slug,
        context: {},
      })
    })
  })
}
```

Pour chaque fichier Markdown dans le répertoire **content**, cette fonction va dans un premier temps chercher certaine données comme **path** et **slug** dans le contenue du fichier via une query graphql. Dans un second temps ces données vont être envoyé à un composent React.js "template" (ici dans le fichier _src/templates/blog-post.js_) qui sera en charge de la création de la page et de son design:

```javascript
export default ({ data }) => { 
  // data est le resultat de la requête sql en dessous.
  // Gatsby est en charge d'éxécuter la requête et de donner le résultat
  // au composant. Pas besoin de s'en préoccuper.
  const post = data.markdownRemark
  return (
    <Layout>
      <SEO
        title={post.frontmatter.title}
        description={post.frontmatter.description || post.excerpt}
      />
      <Content>
        <MarkedHeader>{post.frontmatter.title}</MarkedHeader>
        <HeaderDate>
          {post.frontmatter.date} - {post.fields.readingTime.text}
        </HeaderDate>
        <MarkdownContent dangerouslySetInnerHTML={{ __html: post.html }} />
      </Content>
    </Layout>
  )
}

export const pageQuery = graphql`
  query($path: String!) {
    markdownRemark(frontmatter: { path: { eq: $path } }) {
      html
      excerpt(pruneLength: 160)
      frontmatter {
        date(formatString: "DD MMMM, YYYY")
        path
        title
      }
      fields {
        readingTime {
          text
        }
      }
    }
  }
`
```
Le concept est le même ici. Gatsby va chercher de l'information dans le fichier Markdown via une requête sql et créer le blog post à partir de ces données. Quelques soit votre source de données, graphql sera votre langage pour questionner ces dernières. Et comme graphql est un langage simple et flexible, ce n'est pas pour nous déplaire. C'est à mon avis un des plus gros avantage de Gatsby. Cette logique me permet d'avoir une multitude de fichier Markdown différents avec du contenue différent, mais un seul composant React.js de quelques lignes pour les afficher (je vous ai épargné du CSS ici car ça à peu d'intérêt dans cet article.)

Et ce concept est le même pour chaque page. Regardons maintenant le contenue de _culture-matters.js_, qui est la page d'accueil de la section culture:

```javascript
const IndexPage = ({ data }) => {
  // Même chose que blog-post.js, les données extraite sont directement injecté ici.
  return (
    <Layout>
      <SEO title="Culture Matters" />
      <Content>
        <h1>Culture Matters</h1>
        {data.allMarkdownRemark.edges.filter(
          ({node}) => node.frontmatter.path.includes("culture-matters")
        ).map(({ node }) => (
          <div key={node.id}>
            <Link
              to={node.frontmatter.path}
              css={css`
                text-decoration: none;
                color: inherit;
              `}
            >
              <MarkerHeader>{node.frontmatter.title} </MarkerHeader>
              <div>
                <ArticleDate>{node.frontmatter.date}</ArticleDate>
                <ReadingTime> - {node.fields.readingTime.text}</ReadingTime>
              </div>
              <p>{node.excerpt}</p>
            </Link>
          </div>
        ))}
      </Content>
    </Layout>
  )
}

export default IndexPage

export const query = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }
    allMarkdownRemark(sort: { fields: [frontmatter___date], order: DESC }) {
      totalCount
      edges {
        node {
          id
          frontmatter {
            title
            date(formatString: "DD MMMM, YYYY")
            path
          }
          fields {
            slug
            readingTime {
              text
            }
          }
          excerpt
        }
      }
    }
  }
`
```

Le composant React.js boucle sur chaque fichier Markdown, filtre uniquement ceux qui ont "culture-matters" dans leurs path, et créer un lien direct vers l'article en question en affichant quelques données supplémentaires obtenues via une requête graphql:
*  le titre de la section
*  une liste des articles de "culture-matters" avec pour chacun d'entre eux le titre, le temps de lecture, etc... 

Pour finir, un détour rapide par un fichier markdown: 

```markdown
---
title: "Hello World"
date: "2019-08-25"
draft: false
path: "/tech-hub/hello-world"
---

![alt text](https://images.unsplash.com/photo-1500576992153-0271099def59?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80 "Logo Title Text 1")
_Photo de Vladislav Klapin sur Unsplash_

--- 

J'ai toujours voulu écrire pour partager mes découvertes et mes passions. D'abord comme journalist culturel pour montrer mes coups de coeurs, puis comme scientifique, pour diffuser mes trouvailles. Considérez cce blog comme un moyen d'assouvir cette passion. Mais qu'allez vous trouver sur ce blog ?
...
```

Les metadatas d'un post comme le titre ou son path sont entre les deux premier **---**. Gatsby est capable de les intérpréter comme tel et de ne pas les inclure dans le contenue même de l'article. Mais elle sont néanmoins utilisable par graphql. 

##  Pour conclure

J'aime Gatsby car je trouve que c'est l'outils parfait pour construire les sites static aux contenue dynamique. Il offre une couche d'abstraction agréable à React.js mais suffisamment configurable pour pouvoir customiser son site à l'envie. Cette article ne rentre pas dans les détails de Gatsby.js pour deux raisons: je souhaitais ici présenter seulement la logique de l'outils, et aussi je ne le connais pas encore assez bien pour écrire un article plus complet. Mais il me reste du travail sur ce site (mise en place de commentaires, plus de composants pour la section encore vide de portofolio, etc...). Je vais donc pouvoir le pratiquer et refaire un article plus complet dessus. 

Pour la précision, j'utilise GitLab comme outils de versioning. Vous trouverez le repo [ici](https://gitlab.com/kevinl31/personal-blog). N'hésitez pas à le forker pour créer votre propre site. J'utilise [Netlify](https://www.netlify.com/) pour le déploiement du blog. Le flow de déploiement sera aussi le sujet d'un autre article (petit aussi celui la tellement Netlify rend ça facile). 

J'espère que cet article vous donnera envie d'en savoir plus sur Gatsby.js, car ça vaut le coup. Ca serait un vrai plaisir de lire d'autre blog bâtis avec. En attendant, restez curieux 😉 ! 