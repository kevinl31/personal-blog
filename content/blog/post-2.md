---
title: "Introduction au développement dirigé par les tests"
date: "2019-09-01"
draft: false
path: "/tech-hub/test-driven-development-part-1"
---

Le but de cet article est de montrer comment mettre en pratique le _Test Driven Development_ (TDD) et surtout de vous montrer comment penser la rédaction de son code avec la logique du TDD. Les exemples qui parsèmeront le billet seront en Python.

Le projet sur lequel je j'ai m'appuyer pour mes démonstration est un projet très naïf de Calculatrice (très original ^^) capable de réaliser des additions et des soustractions. La logique sera primaire mais suffisante pour illustrer la philosophie du TDD.

![photo unsplash](https://images.unsplash.com/photo-1518349619113-03114f06ac3a?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80 "Unit test unsplash Picture")
_Photo de David Travis sur Unsplash_

---

## La philosopphie du TDD

### Les tests unitaires sont importants

Un mauvais codeur va développer une nouvelle fonctionnalité et simplement pusher son code vers on outils de versioning (ne faite pas ça!). Un meilleur codeur va déveloper la même fonctionnalité, écrire quelques tests unitaires pas la suite, et si tous ces tests passent, pusher son code. Les tests unitaires sont importants pour plusieurs raisons: 

- vous êtes sur que le code à le comportement attendu parce que vous tester ce dernier
- votre code est plus stable. Si vous venez à coder une nouvelle fonctionnalité, les tests unitaires vous permettrons de tester la non régression de votre code.
- votre code est plus scalable. Un nouveau développeur arrivant dans votre équipe peut facilement comprendre un projet en lisant ses tests unitaires pour comprendre le comportement d'une fonction, d'une classe, etc...

Dans une logique de développement normal, les tests sont écrits après le code.

### TDD sera strict, mais juste !

En revanche avec le _Test Driven Development_, les tests doivent être écrit **avant**. Ils deviennent une sorte de contrat que vous passez avec votre future fonctionnalité: "je veux pouvoir ajouter deux nombres et obtenir le bon résultat". Ensuite, vous écrivez le code qui va remplir votre contrat. Vous éxécuter les tests, et si ces derniers passent sans erreur, alors votre code remplit le contrat. Sinon, vous devez itérer: changer votre code, réexécuter vos tests, etc... jusqu'à ce que votre contrat soit remplis.

Une chose importante: Vous ne pouvez pas changez vos tests en court de route. Si vous faite ainsi, alors vous serez tenté de modifier votre contrat passé avec la nouvel fonctionnalité pour qu'il match avec votre code potentiellement mal écrit: "Si je n'arrive à ajouter deux nombres via mon code, alors je décrète que ma fonction doit juste retourner un nombre". Ca ne peut pas fonctionner...

Cela implique que chaque fonctionnalité doit venir avec des spécifications techniques et fonctionnelles détaillé avant de commencer l'écriture des tests pour que ces derniers soient le plus complet possible.

C'est une des assertions les plus importante du TDD: **Vous devez être sur de la feature que vous allez développer avant d'écrire vos tests parce qu'idéalement, vous ne pouvez pas les changer. Donc vos spécifications initials doivent être nickel**

Écrire de bonnes specifications demandent en général beaucoup de temps. Mais croyez moi, le temps perdu ici sera largement compensé par le temps gagné pendant le développement et la maintenance du projet. 

## TDD sur notre projet de calculatrice

### D'abord, les Specs

Nous commençons ce projet à partir de rien. Nous avons seulement notre répertoire projet avec 3 sous-repertoire à l'intérieur:

![alt text](https://res.cloudinary.com/dms9ldi2g/image/upload/v1567353591/blog/calculator-folder_hmpk4z.png "Project Folder")

Le code sera dans le repertoire **calculator**, les tests dans **test_calculator** et j'ai ajouté un repertoire **venv** pour notre environnement virtuel python dans lequel sera installé Pytest. C'est le package python que j'utiliserai pour écrire les tests unitaires.

Les deux fonctionnalité que je cherche à implémenter sont l'addition et la soustraction. Les spécifications pourraient être: 

- Je veux que ma fonction d'addition prenne en entré _a_ et _b_ et qu'elle me retourne le bon résultât. _a_ et _b_ peuvent être des int, des float ou des string (on peut assimiler l'addition de deux string à leurs concatenation).
- Un message d'erreur doit être levé si _a_ ou _b_ n'est ni un int, ni un float, ni une string.
- Je veux que ma fonction de soustraction prenne en entré _a_ et _b_ et qu'elle me retourne le bon résultât. _a_ et _b_ peuvent être des int ou des float.
- Un message d'erreur doit être levé si _a_ ou _b_ n'est ni un int, ni un float.

Ces spécifications paraissent naïves et évidentes. Mais dans certains cas, elles peuvent être bien plus complexes, accompagné de schémas et de diagrams. Au plus vos spécifications sont complètes, au plus vos tests seront précis et votre code de bonne qualité.

### Écrivons notre contrat

Maintenant que nos spécifications sont écrites, écrivons nos tests:

```python
# test_calculator/test_compute.py
import pytest

from calculator.compute import add, sub

def test_add():
    assert add(1,1) == 2 # basic test
    assert add(45.8, 54) == 99.8 # test with float
    assert add(-12, 36) == 24 # test negative value
    assert add("Hello", " World!") == "Hello World!" # test with str

    with pytest.raises(TypeError): 
        add([1, 2, 3], 40) # test with non str, int or float value


def test_sub():
    assert sub(1,1) == 0 # basic test
    assert sub(45.8, 54) == pytest.approx(-8.20) # test with float
    assert sub(-12, 36) == -48 # test negative value

    with pytest.raises(TypeError): 
        sub("Hello", 40) # test with non numeric value
```
`assert` est un mot-clé python qui lève une exception AssertionError si l'expression booléenne qui suit retourne False, et qui ne fait rien si elle retourne True. Nous pouvons donc considérer que nos tests passent aucune exception n'est levé. `with pytest.raises(TypeError)` nous permet de tester que la fonction retourne bien une exception _TypeError_. 

Pour executer nos tests, nous devons installer Pytest et l'exécuter:

```bash
# activation of your virtualenv
source venv_calculator/bin/activate
# pytest installation
pip install pytest
# test execution
python -m pytest
```

Si vous lancez la dernière commande, les tests seront en erreur immédiatement, et c'est normal: nous n'avons pour le moment écrit aucune ligne de code. C'est le concept clé du TDD: **Nos futurs développements seront directement dicté par nos tests (le contrat) que nous venons d'écrire.

## Notre contrat écrit, codons

Voici le code des méthodes _sub()_ et _add()_ qui remplissent le contrat que nous venons d'écrire:

```python
# calculator/compute.py
def add(a, b):

    if not isinstance(a, (int, float, str)):
        raise TypeError("a is not an int or a float.")

    if not isinstance(b, (int, float, str)):
        raise TypeError("b is not an int or a float.")

    return a + b


def sub(a, b):

    if not isinstance(a, (int, float)):
        raise TypeError("a is not an int or a float.")

    if not isinstance(b, (int, float)):
        raise TypeError("b is not an int or a float.")

    return a - b
```
Maintenant, si vous exécuter vos tests avec `python -m pytest`, ils passent tous. Nous avons complété notre contrat. Nos nouvelles fonctionnalité sont prêtes

## Attendez une minutes...

Que ce passe t'il si nous décidons d'ajouter un int et un string dans notre fonction add() avec par exemple `add(10, "Hello")` ? Es-ce que la méthode devrait nous renvoyer un résultat ? Ou nous lever une erreur car c'et étrange de vouloir addition un int et un string ? Es-ce que nous devons essayer de transformer l'int en string et ensuite faire une concatenation de deux string ? Nos tests unitaire ne couvrent pas ce cas particulier. Et c'est normal, puisque nous ne l'avons pas éluder dans nos spécifications. Elles ne sont pas assez détaillées. **En vérité, elles ne seront jamais assez détaillées. Mais plus vous pratiquerez le TDD, meilleurs elles seront, et meilleurs sera votre code**. Alors comment gérer ce cas ici ? Je pense bien entendu que c'est un cas particulier qui devrait lever une exception dans le cadre de notre projet. Mais nous ne le testons pas. Ici, cette situation est très naïve, et n'importe quel développeur avec un peu d'éxpérience ce serait posé la question lors de l'écriture des tests. Mais pensez à des projet plus complexe ou l'oublie de quelques détails dans les spécifications peut mener à des failles importantes dans le projet final. Conseil: Essayer d'écrire les spécifications à deux. C'est souvent utile d'avoir plusieurs point de vue.  

## Pour résumer

- les tests unitaires augmentent globalement la qualité de votre code et ça scalabilité.
- le TDD est une méthode qui consiste à écrire vos tests (le contrat) avant votre code. Le développement est terminé quand le contrat est rempli.
- le TDD implique de définir avec détails ces spécifications techniques et fonctionnelles 
- Évitez de tester une même chose deux fois. C'est une perte de temps et vous perdez en lisibilité de vos tests
- Dans l'écosystème Python, Pytest est l'un des meilleurs framework de tests unitaires. Un autres article lui sera dédié.

Les tests unitaires et la philosophie du _Test Driven Development_ peuvent être utilisé dans d'autre contexte, comme le développement web ou le Machine learning avec quelques adaptations. Ce sera le sujet de futures articles. En attendant, bon tests, et restez curieux 😉 ! 

## Pour aller plus loin

- [TDD Changed My Life](https://medium.com/javascript-scene/tdd-changed-my-life-5af0ce099f80)
- [TDD the RITE Way](https://medium.com/javascript-scene/tdd-the-rite-way-53c9b46f45e3)
- [Rethinking Unit Test Assertions](https://medium.com/javascript-scene/rethinking-unit-test-assertions-55f59358253f)
- [Pytest documentation](https://docs.pytest.org/en/latest/contents.html)