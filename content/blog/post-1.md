---
title: "Hello World"
date: "2019-08-25"
draft: false
path: "/tech-hub/hello-world"
---

![alt text](https://images.unsplash.com/photo-1500576992153-0271099def59?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80 "Logo Title Text 1")
_Photo de Vladislav Klapin sur Unsplash_

--- 

J'ai toujours voulu écrire pour partager mes découvertes et mes passions. D'abord comme journaliste culturel pour montrer mes coups de coeur, puis comme scientifique, pour diffuser mes trouvailles. Considérez ce blog comme un moyen d'assouvir cette passion. Mais qu'allez-vous trouver sur ce blog ?

Dans la section **Tech Hub**, vous pourrez lire des articles techniques parlants de code, d'outils de développeur et de techniques de développement. D'abord parce que je me vois un peu comme un geek, toujours à l'affût d'une nouveauté dans le monde vaste de l'informatique, ensuite parce que j'ai beaucoup appris sur les blogs d'autres codeurs. Je pense que c'est à mon tour de partager ces connaissances. Les articles que vous lirez tourneront surtout autour de technos que je maîtrise et que j'utilise au quotidien (Python, Node.js, Docker, etc...) mais il n'est pas impossible que vous y trouviez des articles sur des sujets techniques qui sortent de ma zone de confort.

Puis vous trouverez dans la section "Culture Matters" plusieurs articles sur des produits culturels que je découvre au jour le jour et qui me semblent intéressant de partager avec vous. Je parlerai ici des livres, films, expositions, albums, jeux vidéo, pièce de théâtre, et tout autre objet culturel que j'aime au point de vouloir en parler. N'étant pas critique, je me garderai de les juger. Je partagerai seulement ce qui me semble bien et j'essayerai au mieux d'expliquer pourquoi l'oeuvre m'a touché.

Enfin vous trouverez dans la section portfolio des projets que j'ai réalisés, que j'ai abandonné, sur lesquels je travail ou encore sur lesquels j'aimerais pouvoir m'investir.

J'espère que vous trouverez votre bonheur parmi les articles de ce blog. Et si c'est le cas, n'hésitez pas à le partager avec vos amis. Bonne lecture, et restez curieux 😉 !