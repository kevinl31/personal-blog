---
title: "Culture 2000"
date: "2019-09-15"
draft: false
path: "/culture-matters/culture-2000"
---

Le monde du podcast a explosé en France dans les 5 dernières années. Alors qu'ils étaient anecdotiques il y a peut, on a aujourd'hui sur n'importe quelle plateforme musicale pléthore de choix pour trouver la bonne émission. Et pour moi, une de ces très bonnes émissions, c'est Culture 2000.

!["Logo de Culture 2000"](https://res.cloudinary.com/dms9ldi2g/image/upload/v1568479986/blog/culture-2000-logo_bkcl5k.jpg "Logo de Culture 2000")

On y parle d'Histoire. Si vous n'avez pas envie de lire une dizaine de livres sur la guerre d'Indochine, l'indépendance de l'Irlande ou encore la naissance du christianisme, alors ce podcast est pour vous. En effet, l'équipe arrive à donner un résumé assez fourni sur un sujet historique en une petite heure. Les connaissances apportées sont suffisantes pour pouvoir faire semblant dans une discussion mondaine ou pour avoir les clés nécessaires pour ensuite approfondir le sujet "solo".

L'équipe est composé de Marlène, la responsable géographique, Yoan, Jean-Baptiste, Lea la "voyante" et Greg le chef d'orchestre de toute ce petit monde. Le ton n'est intelligent, assez à gauche, plein de blagues et jamais ennuyant. Chacun à son rôle.

Ils avouent parfois ne rien connaître au sujet avant de commencer à bosser un épisode. Car oui, chaque épisode est travaillé par les membres (d'où une sortie l'une ou l'autre semaine), et ça se voit. Ils les ponctuent d'anecdotes, de références, et parfois de réflexions qu'ils ont eus pendant leurs recherches. C'est assez cadré, avec une vraie trame qui suit généralement un ordre chronologique. Le tout donne des podcasts riches où l'on est sûr d'apprendre un truc.

Les sujets sont très variés. Ils parlent de choses assez "sérieuse" comme la révolution russe de 1917, et de sujet un peu plus léger comme l'histoire du Ricard. Mais ils sont tous traité avec maîtrise. Et on arrive même à apprendre des choses sur l'histoire de France au 20ème dans l'épisode sur le Ricard (je vous laisse l'écouter).

À noter que ce podcast fait partie d'un collectif de podcast appelé **Fréquence Moderne** est composé d'autres bonnes émissions. Je pense notamment à **EPO** et **2 heures de perdues** qui feront l'objet de futurs articles. En attendant, je vous souhaite bonne écoute et surtout, restez curieux 😉 !