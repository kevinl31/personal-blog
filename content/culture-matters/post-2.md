---
title: "Iris, A Space Opera par Justice"
date: "2019-09-01"
draft: false
path: "/culture-matters/iris-a-space-opera-by-justice"
---

Il y a quelques jours, j'ai vu au cinéma **Iris, A Space Opera par Justice**, et je pense que c'est une des meilleur performances qu'il m'ai été permis de voir. 

![Iris poster](https://res.cloudinary.com/dms9ldi2g/image/upload/v1567365333/blog/justice_iris_elkio5.jpg "Iris poster")

Justice est un groupe Français d'électro créé en 2003 et composé de Gaspard Augé et de Xavier de Rosnay. Je les ai découvert par hasard, lors d'un voyage en voiture, lorsqu'un de mes amis à diffuser ma musique préférée du groupe: Genesis. Cette chanson est difficilement descriptible. Mais dès l'introduction, elle pose une ambiance angoissante qui vous prend aux tripes et qui vous emmène dans leur univers. J'ai tout de suite accroché. 

Mais je ne me suis jamais considéré comme un grand fan de Justice. J'écoutais de temps en temps leurs musiques, mais sans plus. Quand j'ai vu le trailer de Iris, ma curiosité a été piqué. Qu'est-ce que peut donner une performance live faite pour le cinéma ?

Les 2 amis ont essayé plusieurs fois dans leur carrière d'enregistrer leurs concerts, mais ont toujours été déçu du résultat. De leur point de vue, un concert se compose de deux choses: une performance technique (musical et scénographique) et une synergie avec le public. Il est difficile de capter les deux en même temps. Pour Gaspard et Xavier, les moments d'interaction avec le public et la communion qu'ils ont avec leurs fans est quelque chose qui lors du concert est incroyable, mais qui pollue l'enregistrement, ces émotions étant dures à transmettre à travers une télé. Ils ont donc eu l'idée de faire un show sans public complètement penser pour le cinéma.

Le résultat est bluffant. Le show est celui dérivé de leur album **Woman**. **Iris, a Space Opera**, c'est 60 minutes de pure performance. J'ai eu la chance de le voir dans un contexte privilégié (dans un cinéma), mais tout est fait pour avoir un rendu parfait.

D'abord la musique. Alors bien sûr il faut aimer un minimum Justice et les sons Electro/Pop/EDM pour être conquis. Ils arrivent à mixer parfaitement leurs classiques (D.A.N.C.E, Genesis) et les musiques de leur dernier album. Les enchaînements sont bons, il n'y a pas de temps mort, et on se surprend même à bouger la tête et les jambes en rythme, alors qu'en général l'ambiance dans une salle de ciné ne s'y prête pas. Ils arrivent à asseoir une atmosphère qui match parfaitement avec la scénographie et les jeux de lumière.

La scénographie justement. Elle est conçue de manière à nous faire croire, l'espace de 60 minutes, que nous sommes dans un vaisseau spacial. On retrouve sur scène les grands symboles associés à Justice: la croix et le mur d'enceinte Marshall.  

![Justice Cross](https://i.ytimg.com/vi/VKzWLUQizz8/maxresdefault.jpg "Justice Cross")

Sur scène, les deux hommes sont entourés de machines, bougeant durant tout le show autour des 2 musiciens. Ces machines renforcent l'impression d'être dans l'espace. On visualise vraiment le vaisseau du premier Alien, ou des premiers Star Wars, avec leur attirail d'équipement analogique. Le sol quand à lui, est noir laqué, permettant au reste de se réfléchir légèrement dedans. Cela donne une incroyable profondeur à l'ensemble.

Enfin le jeu de lumière. C'est ce qui m'a le plus scotché. Il y a un nombre colossal de lumière partout sur la scène, porté par les machines et tournant autour de la scène en permanence. Elles sont parfaitement synchronisées à la musique (vous penserez à moi lors du départ de la chanson Genesis). Les mouvements de caméra sont lents, permettant d'en profiter le plus possible. C'est un choix de l'équipe pour nous laisser le temps d'admirer chaque plan du film. Orange, Rouge, Jaune et Blanche, les lumières dansent sur le rythme du son. Elles sont parfois douces, souvent nerveuses et aveuglantes, mais servent à 100% le propos du show. Leurs reflets dans le sol réfléchissant sont superbes.

Pour résumer, c'est une vraie claque que j'ai prise. Je dis souvent être plus facilement touché par la musique que par le cinéma, mais lorsque le dernier se met au service de la première, le résultat est tout simplement bluffant. Ça me fait espérer que d'autres groupe décide de faire la même chose. En y réfléchissant, je pense que la musique électro, avec le principe du mix et de l'enchaînement des chansons se prête bien à l'exercice. Je ne sais pas si d'autre style musical comme le Rap ou le Pop-Rock arriverait à créer un ensemble aussi structuré et cohérent, car le mixage et l'enchaînement des chansons semblent plus difficiles. Je ne demande qu'à voir un autre film de ce genre qui me fasse mentir.  

Malheureusement, il semble que c'était une représentation unique du film. Vu le succès, je pense que d'autres dates seront prévues. Si c'est le cas, arrêtez tout, et courrez le voir en salle. Ça vaut le coup. En attendant, une de leurs prestations live (en public cette fois) est disponible sur [youtube](https://www.youtube.com/watch?v=HpsQ9GuzIGw&t=1183s). Allez-y jeter un coup d'ceil, et restez curieux 😉 !

Link:

- [Justice Wikipédia](https://fr.wikipedia.org/wiki/Justice_(groupe))
- [Site officiel d'Iris, A Space Opera by Justice](https://fr.justice-iris.film/)