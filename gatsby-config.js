module.exports = {
  siteMetadata: {
    title: `🖥 Code and Culture 🎶`,
    description: `
    Bienvenue sur Code and Culture, blog de tech et de recommandation culturel. Je suis Kévin, développeur Python,
    adepte du Node.js et passionné de Data Engineering. Je suis aussi un amoureux de culture et d'art sous toutes ses formes.`,
    notice: `Vous trouverez ici des articles tech dans la section Tech Hub et des produits 
    culturels que je pense intéressant de partager avec vous dans l'onglet Culture Matters. Bonne lecture et restez à l'affût des nouveaux contenus !`,
    author: `@kevinLtp59`,
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `markdown-pages`,
        path: `${__dirname}/content`,
      },
    },
    // {
    //   resolve: `gatsby-source-filesystem`,
    //   options: {
    //     name: `images`,
    //     path: `${__dirname}/content/images`,
    //   },
    // },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    `gatsby-plugin-emotion`,
    {
      resolve: `gatsby-transformer-remark`,
      options: {
        plugins: [`gatsby-remark-reading-time`, {
          resolve: `gatsby-remark-prismjs`,
          options: {
            aliases:{sh: "bash", js:"javascript"},
            showLineNumbers: true,
          }
        }],
      },
    },
    {
      resolve: `gatsby-plugin-netlify`,
      options: {
        headers: {
          "/*": [
            "Strict-Transport-Security: max-age=63072000"
          ]
        }, // option to add more headers. `Link` headers are transformed by the below criteria
        allPageHeaders: [], // option to add headers for all pages. `Link` headers are transformed by the below criteria
        mergeSecurityHeaders: true, // boolean to turn off the default security headers
        mergeLinkHeaders: true, // boolean to turn off the default gatsby js headers
        mergeCachingHeaders: true, // boolean to turn off the default caching headers
        transformHeaders: (headers, path) => headers, // optional transform for manipulating headers under each path (e.g.sorting), etc.
        generateMatchPathRewrites: true, // boolean to turn off automatic creation of redirect rules for client only paths
      },
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `A Python In Paris`,
        short_name: `Payris`,
        start_url: `/`,
        background_color: `#DED3C0`,
        theme_color: `#DED3C0`,
        display: `minimal-ui`,
        // icon: `content/images/snake-favicon.png`
      },
    },
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // 'gatsby-plugin-offline',
  ],
}
